Web Data Processing Systems
====
Entity Linking
---
___

###Design - Overview

* Step 1 is to extract only the web page text from the WARC, for each document by parsing, using "WARC/1.0" as the document separator.

* Step 2 is to extract text from HTML tags. We could remove all tags and keep only the text or keep only some type of tags and their corresponding text in a dictionary form. E.g. keep the links separetely from the body's text, to be able to use them. 

* Step 3 is to pass the text to an NER plugin to give us the recognised entities and their classes. It would also be useful to have the frequency of each entity in this text.

//This requires more search ----

* Step 4 is for each entity to construct a feature vector. 
    1. Idea : the frequency of appearance of each entity inside a document maybe used to indetify the entities most relevant to the document. The entities with a frequency closer to the average or the ones with the highest frequences can be considered as the "topic" of the document which can be used to narrow searches in the KB. In this case, "closer" and "the ones with highest" must be defined. This set of entities could be stored in the feature vector for further analysis if needed.

* Step 5 is to query the KB for each entity and get possible entities that match and for each of them construct a feature vector. 

* Step 6 is to check similarity of vectors and decide which entity to match to. 

