from pyspark import SparkContext
import sys
import collections
import sys, re
from bs4 import BeautifulSoup
import preprocessing
from entity_query import link_entity
from nltk.corpus import wordnet, stopwords
import threading

KEYNAME = "WARC-TREC-ID"
ELS_DOMAIN, SPQL_DOMAIN, wordnet, stopwords = "", "", "", ""

def return_text(html_doc):
    soup = BeautifulSoup(html_doc, "html.parser")
    data = soup.findAll(text=True)

    def visible(element):
        if element.parent.name in ['style', 'script', '[document]', 'head', 'title', 'meta', 'link']:
            return False
        elif re.match('<!--.*-->', str(element.encode('utf-8'))):
            return False
        elif element == " ":
            return False
        return True

    result = filter(visible, data)
    result = (" ".join(result))
    return result

def remove_headers(doc_input):
    lines = doc_input.splitlines()
    start_reading = False
    html_text = ""
    for line in lines:
        if start_reading == False:
            if line == "":
                start_reading = True
        else:
            html_text += line
    html_text1 = return_text(html_text)
    #print(html_text1)
    #print
    #print("========================================")
    return html_text1

#Each thread is responsible for one document
def worker(record):
        print "checkpoint---------------"
        """
        wid = record["WARC-Record-ID"]
        doc_id = wid.split(":")
        doc_id = doc_id[2]
        doc_id = doc_id.split(">")
        doc_id = doc_id[0]
        """
        payload = record
        key = None
        for line in payload.splitlines():
            if line.startswith(KEYNAME):
                key = line.split(': ')[1]
                break

        doc = remove_headers(payload)

        #preprocessing for non empty documents
        if doc:
            document_info = preprocessing.preprocess(doc, wordnet, stopwords)

            #get nouns form test
            entity_text = [noun.encode("utf-8") for noun in document_info.nouns];

            #get entities and their types and call link_entity
            for num in range(len(document_info.entities)):
                entity_name = document_info.entities[num][0].encode("utf-8")
                entity_type = document_info.entities[num][1].encode("utf-8")
                fb_id = link_entity(ELS_DOMAIN, SPQL_DOMAIN, entity_name, entity_type, entity_text, wordnet, stopwords)
                if fb_id is None:
                    fb_id = "00000000"

                print (doc_id, "    ", entity_name, "    ", fb_id)

"""
def find_google(record):
    # finds google
    _, payload = record
    key = None
    for line in payload.splitlines():
        if line.startswith(KEYNAME):
            key = line.split(': ')[1]
            break
    if key and ('Google' in payload):
        yield key + '\t' + 'Google' + '\t' + '/m/045c7b'
"""

if __name__ == '__main__':

    try:
        _, ELS_DOMAIN, SPQL_DOMAIN, INFILE, OUTFILE = sys.argv
    except Exception as e:
        print('Error: '+ str(e))

    wordnet.ensure_loaded()
    stopwords.ensure_loaded()

    sc = SparkContext("yarn", "wdps1815")

    #KEYNAME = "WARC-TREC-ID"
    #INFILE = sys.argv[1]
    #OUTFILE = sys.argv[2]

    rdd = sc.newAPIHadoopFile(INFILE,
        "org.apache.hadoop.mapreduce.lib.input.TextInputFormat",
        "org.apache.hadoop.io.LongWritable",
        "org.apache.hadoop.io.Text",
        conf={"textinputformat.record.delimiter": "WARC/1.0"})


    rdd = rdd.flatMap(worker)

    rdd = rdd.saveAsTextFile(OUTFILE)
