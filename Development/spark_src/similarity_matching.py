from difflib import SequenceMatcher


#Function to comaper two list of stings
def similar(a, b):

    score = 0
    count = 0
    for word in a:
        count +=1
        for word2 in b:
            if SequenceMatcher(None, word, word2).ratio() >= 0.8:
                score += 1
                break    
    if(score > 0):    
        totalScore = float(score) / count
        return totalScore
    return 0

if __name__ == '__main__':
    #filename = sys.argv[1]

    try:
        #print(similar(['want','to', 'get', 'decimals', 'value'], ['want', 'top', 'got',  'decimal', 'value']))

        #print(similar(['want','to', 'get', 'decimal', 'value'], ['but', 'there', 'are', 'multiple', 'similarity', 'metrics']))
        print(similar(["congressperson", "representative","position","politician","extra","person","schemastaging","u","common","physically","instantiable","poldb","us","topic","animate","current","","held","government", "people", "agent"], set("Barack Hussein Obama About this soundlisten born August 4, 1961 is an American attorney and politician who served as the 44th President of the United States from 2009 to 2017. A member of the Democratic Party, he was the first African American to be elected to the presidency. He previously served as a United States Senator from Illinois")))

    except IOError:
        print("Error opening file")
