#!/usr/bin/env bash

# Usage: bash spark_sample.sh [INFILE] [OUTFILE]
SCRIPT=${1:-"linker.py"}
INFILE=${2:-"hdfs:///user/wdps1815/sample.warc.gz"}
OUTFILE=${3:-"sample"}

PYSPARK_PYTHON=$(readlink -f $(which python)) ~/scratch/webdataprocsys/Spark/spark-2.4.0-bin-without-hadoop/bin/spark-submit \
--master yarn $SCRIPT $INFILE $OUTFILE

hdfs dfs -cat $OUTFILE"/*" > $OUTFILE
