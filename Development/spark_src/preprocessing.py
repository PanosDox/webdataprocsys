# the function preprocess returns the nouns of the html_text
#it does lemmatization, depending on NER tagging in order to convert uppercase letters to powercase for the words that are not PERSON, ORGANIZATION OR LOCATION and
# then it does POS tagging in order to retrieve only the nouns

from nltk.tag import StanfordNERTagger, StanfordPOSTagger
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
#from nltk.corpus import stopwords, wordnet
from collections import Counter
import string

'''
import nltk
nltk.download('stopwords')
nltk.download('wordnet')
nltk.download('punkt')
'''
NER_words = {"PERSON", "LOCATION", "ORGANIZATION"}

class EntitiesAndNouns:
    def __init__(self, entities, nouns):
        self.entities = entities
        self.nouns = nouns

    def getEntities(self):
        return self.entities

    def getNouns(self):
        return self.nouns

#using wordnet library to distinguish all the nouns given by POS tagging
def get_lemma_pos(treebank_tag, wordnet):
    if treebank_tag.startswith('N'):
        return wordnet.NOUN
    #elif treebank_tag.startswith('V'):
    #    return wordnet.VERB
    #elif treebank_tag.startswith('J'):
    #    return wordnet.ADJ
    #elif treebank_tag.startswith('R'):
    #    return wordnet.ADV
    else:
        return 'no tag'

###### lemmatization depending on POS tagging ###########
# we can use it with NER for better results
def lemma_with_POS(tokens, stPOS, wordnet):
    #POS tagging (using stanford-POSTagger)
    POS_tagged = stPOS.tag(tokens)				#POS tagging
    POS_tagged_utf8 = [[word.encode('utf-8') for word in tuples] for tuples in POS_tagged] #encode to utf-8 for represantation

    #lematization depending on POS tagging
    lemma_tokens = []
    if POS_tagged_utf8:         #if list is not empty
        lemmatizer = WordNetLemmatizer()
        for token in POS_tagged_utf8:
            if get_lemma_pos(token[1], wordnet) == 'no tag':
                lemma_tokens.append(lemmatizer.lemmatize(token[0].decode('utf-8')))
            else:
                lemma_tokens.append(lemmatizer.lemmatize(token[0].decode('utf-8'), get_lemma_pos(token[1], wordnet)))
        #lemma_tokens = [lemmatizer.lemmatize(token[0], get_lemma_pos(token[1])) for token in POS_tagged_utf8]

    return lemma_tokens
###############################################################

########################  Return Entities  ##################
# INPUT: a list such as ("word","NER result")
# OUTPUT: return entities , join many word entities as one entity  (e.g. "Vrije University")
def getEntities(NER_tagged):
    Entities = []
    tempEntity = ""
    previousNER = ""
    for tuples in NER_tagged:
        if (tuples[1] == previousNER):
            tempEntity += " "
            tempEntity += tuples[0]
        elif tuples[1] in NER_words:
            tempEntity = tuples[0]
            previousNER = tuples[1]
        else:
            if (previousNER != ""):
                 tempTuple = (tempEntity, previousNER)
                 Entities.append(tempTuple)
            tempEntity = ""
            previousNER = ""

    return Entities
#################################################################



def preprocess(doc, wordnet, stopwords):

    #Training models  (we should check other classifiers too)
	#change path
    stNER = StanfordNERTagger('../classifiers/english.all.3class.distsim.crf.ser.gz', '../stanford_JAR_files/stanford-ner.jar', encoding='utf-8')
    stPOS = StanfordPOSTagger('../models/english-bidirectional-distsim.tagger', '../stanford_JAR_files/stanford-postagger.jar', encoding = 'utf-8')

    tokens = word_tokenize(doc)    	    #tokenize

    ######## NER tagging  ############ (using stanford-NER)
    NER_tagged = stNER.tag(tokens)       	#NER
    NER_tagged_utf8 = [[word.encode('utf-8') for word in tuples] for tuples in NER_tagged] #encode to utf-8 for represantation
    ##########################################

    # Get entities calling function getEntities()
    Entities = getEntities(NER_tagged_utf8)


    ############   remove annoying words  #############
    words_to_remove = {'rss', '\xc2\xa7', '\xa7'}
    #tokens = [token for token in tokens if token not in words_to_remove]
    newNER_tagged_utf8 = []
    for tuples in NER_tagged_utf8:
        if tuples[0] not in words_to_remove:
            newNER_tagged_utf8.append(tuples)

    NER_tagged_utf8 = newNER_tagged_utf8

    ##########  remove stop words  (we may not use it)  #############
    stop = stopwords.words('english')
    #convert stop list from unicode to string
    stop_w = [st.encode('UTF8') for st in stop]
    #tokens = [token for token in tokens if token not in stop]
    newNER_tagged_utf8 = []
    for tuples in NER_tagged_utf8:
        if tuples[0] not in stop_w:
            newNER_tagged_utf8.append(tuples)
    NER_tagged_utf8 = newNER_tagged_utf8
    
    #############  remove punctuations  #################
    #tokens_without_punct = [s for s in tokens if s not in string.punctuation]
    #tokens1 = tokens_without_punct
    newNER_tagged_utf8 = []
    for tuples in NER_tagged_utf8:
        if tuples[0] not in string.punctuation:
            newNER_tagged_utf8.append(tuples)
    NER_tagged_utf8 = newNER_tagged_utf8
    tokens = [tuples[0] for tuples in NER_tagged_utf8]

    #############  convert capital letters to lowercase for nouns  ###########
    for i in range(len(NER_tagged_utf8)):
        if NER_tagged_utf8[i][1] not in NER_words:
            NER_tagged_utf8[i][0] = NER_tagged_utf8[i][0].lower()
            tokens[i] = NER_tagged_utf8[i][0].decode('utf-8')
    ######################################################

    ######## POS tagging  ############
    #POS tagging (using stanford-POSTagger)
    POS_tagged = stPOS.tag(tokens)				#POS tagging
    POS_tagged_utf8 = [[word.encode('utf-8') for word in tuples] for tuples in POS_tagged] #encode to utf-8 for represantation
    ##########################################

    ###### lemmatization depending on POS tagging ###########
    lemma_tokens = lemma_with_POS(tokens, stPOS, wordnet)

    ######### return only nouns ##########
    entity_tagger = []
    noun_tokens = []
    for i in range(len(POS_tagged_utf8)):
        get_POS = get_lemma_pos(POS_tagged_utf8[i][1], wordnet)

        if get_POS == "n":
            #b = lemma_tokens[i], get_lemma_pos(POS_tagged_utf8[i][1]), NER_tagged_utf8[i][1]
            #POS_NER_tagger.append(b)
            noun_tokens.append(lemma_tokens[i])

    # create an object of class extract_doc_info.doc() and store results
    document_info = EntitiesAndNouns(Entities, noun_tokens);

    return document_info  #POS_NER_tagger, noun_tokens

if __name__ == '__main__':
    pass






#############  NOTES ################
# other possible solutions

#tagged = nltk.pos_tag(tokens)				#another POS tagger  (faster but worst results)
#entities = nltk.chunk.ne_chunk(tagged)		#another NER

"""
#split documents in sentences and then into words
sentence = nltk.sent_tokenize(doc)
tokens = [word_tokenize(sent) for sent in sentence]    	#tokenize
"""

"""
#Count same words
types = Counter(noun_tokens)
"""
