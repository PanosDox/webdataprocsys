# This file will contain functionality related to searching for
# candidates matching a query in elastic search and disambiguating
# any results using SPARQL and DBpedia abstracts.

import requests
import json
import re
from similarity_matching import similar
import preprocessing

# This class will describe an entity
class Entity:

    # Give it a name and a freebase id
    def __init__(self, name, fid=""):
        self.name = name    # the entity's name
        self.fb_id = fid    # the freebase id
        self.types = []     # the types of this entity
        self.classes = []    # the classes of this entity
        self.tlabels = []    # labels derived as text from types
        self.ner_type = ""
        self.db_abstract = ""
        self.abstract_nouns = []
        self.label_match_score = 0.0
        self.abstract_match_score = 0.0
        self.name_match_score = 0.0

# Executes a SPARQL query
def sparql_query(domain, query, print_results=False):
    url = 'http://%s/sparql' % domain
    response = requests.post(url, data={'print': True, 'query': query})
    if response:
        try:
            response = response.json()
            if print_results :
                print(json.dumps(response, indent=2))
        except Exception as e:
            print(response)
            raise e
        return response
    else:
        print("SPARQL query failed")


# This function will search for freebase ids matching the given "query"
# and retrieve only some of those that have the maximum score, as calculated by
# elastic search. Some times there's more than one such entity and
# further disambiguation must take place.
def get_closest_candidates(domain, query, print_results=False):
    url = 'http://%s/freebase/label/_search' % domain
    # get the 10 closest candidates from freebase
    response = requests.get(url, params={'q': query, 'size': 10})
    # if the response is OK, get the candidates that have the maximum score
    if response:
        data_json = response.json()
        maximum_score = data_json["hits"]["max_score"]
        candidates_json = data_json["hits"]["hits"]
        closest_candidates = []
        for candidate in candidates_json:
            if candidate["_score"] == maximum_score:
                # store it as an Entity instance
                fb_id = "m." + candidate["_source"]["resource"][3:]
                closest_candidates.append(Entity(candidate["_source"]["label"], fb_id))
            else:
                break
        if print_results:
            for can in closest_candidates:
                print("Candidate : <" + can.name + " , " + can.fb_id + ">")
                for label in can.tlabels:
                    print("    Label : " + label)
        return closest_candidates
    else:
        print("Empty response")


# This function will search for freebase ids matching the given "query"
# and retrieve all up to a maximum size, using elastic search.
def get_all_candidates(domain, query, print_results=False):
    url = 'http://%s/freebase/label/_search' % domain
    # get max 100 candidates from freebase
    response = requests.get(url, params={'q': query, 'size': 50})
    # if the response is OK, get retrieve the candidates
    if response:
        data_json = response.json()
        candidates_json = data_json["hits"]["hits"]
        candidate_entities = []
        for candidate in candidates_json:
            fb_id = "m." + candidate["_source"]["resource"][3:]
            candidate_entities.append(Entity(candidate["_source"]["label"], fb_id))
        if print_results:
            for can in candidate_entities:
                print("Candidate : <" + can.name + " , " + can.fb_id + ">")
                for label in can.tlabels:
                    print("    Label : " + label)
        return candidate_entities
    else:
        print("Empty response")


# This function will take all candidates retrieved by elastic search
# and use SPARQL queries to get the types of these candidates by using
# their freebase ids. It will fill in the types in the Enitity instances.
def get_candidate_types(domain, candidate_entities, print_results=False):
    # This predicate searches for types
    predicate = "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
    for candidate in candidate_entities:
        subject = "<http://rdf.freebase.com/ns/" + candidate.fb_id + ">"
        # limit to 10 results
        query = "select * where { " + subject + " " + predicate + " ?o} limit 10"
        candidates_json = sparql_query(domain, query, False)["results"]["bindings"]
        for cand_js in candidates_json:
            candidate.types.append(cand_js["o"]["value"].replace("http://rdf.freebase.com/ns", ""))
    if print_results:
        for can in candidate_entities:
            print("Candidate : <" + can.name + " , " + can.fb_id + ">")
            for ent_type in can.types:
                print("    Type : " + ent_type)
            for label in can.tlabels:
                print("    Label : " + label)


# This function will be used to generate labels by parsing the entity's types text
def generate_tlabels(candidate_entities, print_results=False):
    for can in candidate_entities:
        for fb_type in can.types:
            # clear out unneeded stuff
            type_label = fb_type.replace("/", "")
            type_label = type_label.replace("base.", "")
            type_label = type_label.replace("type_ontology.", "")
            type_label = type_label.replace("_", ".")
            # generate labels from this type
            label_list = type_label.split(".")
            for lb in label_list:
                can.tlabels.append(lb)
            # convert to a set - avoid duplicates
        can.tlabels = set(can.tlabels)
    if print_results:
        for can in candidate_entities:
            print("Candidate : <" + can.name + " , " + can.fb_id + ">")
            for label in can.tlabels:
                print("    Label : " + label)


# This function gets the abstract description for a candidate (Entity type)
def get_abstract(candidate, spql_domain, print_results=False):
    subject = "<http://rdf.freebase.com/ns/" + candidate.fb_id + ">"
    predicate_same = "<http://www.w3.org/2002/07/owl#sameAs>"
    predicate_abstract = "<http://dbpedia.org/ontology/abstract>"

    query = "select ?abstract  where { ?s " + predicate_same + " " + subject + " . "
    query += " ?s " + predicate_same + " ?o . "
    query += " ?o " + predicate_abstract + " ?abstract . "
    query += " } "

    abstracts = sparql_query(spql_domain, query)["results"]["bindings"]

    #Extract only first abstract
    candidate.db_abstract = ""
    for abst in abstracts:
        text = abst["abstract"]["value"]
        matches = re.search('(\"".*?\"@en")', text)
        if( matches ):
            candidate.db_abstract = matches.group(1)
            break

    if print_results:
        print(candidate.db_abstract)


# Gets the entity NER type
def find_ner_type(candidate, print_results=False):
    if "person" in candidate.tlabels:
        candidate.ner_type = "PERSON"
    elif "business" in candidate.tlabels:
        candidate.ner_type = "ORGANIZATION"
    elif "organization" in candidate.tlabels:
        candidate.ner_type = "ORGANIZATION"
    elif "university" in candidate.tlabels:
        candidate.ner_type = "ORGANIZATION"
    elif "location" in candidate.tlabels:
        candidate.ner_type = "LOCATION"
    else:
        candidate.ner_type = "THING"
    if print_results :
        print("Candidate : " + candidate.fb_id + " Type : " + candidate.ner_type)

#Get nouns from abstract using preprocessing.py (Stanford NLP)
def get_abstract_nouns(candidate, wordnet_lib, stopwords_lib, print_results=False):
        document_info = preprocessing.preprocess(candidate.db_abstract, wordnet_lib, stopwords_lib)

        #get only distinct nouns
        candidate.abstract_nouns = list(set(document_info.nouns))
        '''
        for noun in document_info.nouns:
            if noun not in candidate.abstract_nouns:
                candidate.abstract_nouns.append(noun)
        '''
        if print_results:
            for n in candidate.abstract_nouns: print(n)

def get_candidate_name_score(entity_name, can):
    can.name_match_score = similar(entity_name.split(' '), can.db_abstract.split(' ')[:5])

def remove_duplicates(candidate_list):
    new_list = []
    for can in candidate_list:
        if( not new_list ):
            new_list.append(can)
        else:
            if not list(filter(lambda x: x.fb_id == can.fb_id, new_list)):
                new_list.append(can)
    return  new_list


# This function will execute entity linking for an entity, step by step
def link_entity(els_domain, spql_domain, entity_name, entity_type, entity_text, wordnet_lib, stopwords_lib):

    # 1) get the elastic search candidates first
    #candidate_entities = get_closest_candidates(els_domain, entity_name, False)
    candidate_entities = get_all_candidates(els_domain, entity_name, False)
    if candidate_entities:
        # 2) Extract labels for these candidates
        # for the previous candidates, retrieve their types with SPARQL
        get_candidate_types(spql_domain, candidate_entities, False)

        # generate labels from these types
        generate_tlabels(candidate_entities, False)

        # 3) find NER types for each candidate based on the labels
        # 4) exclude candidates with different types than the entity we wish to match
        same_type_candidates = []
        for can in candidate_entities:
            find_ner_type(can, False)
            if can.ner_type == entity_type:
                same_type_candidates.append(can)

        # Remove duplicates
        #same_type_candidates = list(set(same_type_candidates))
        '''
        #  #) for the remaining candidates check the text of the entity for further
        # similarity with the labels of the candidates and assign scores based on this
        for can in candidate_list:
            can.label_match_score = similar(can.tlabels, entity_text)
            print("ID : " + can.fb_id + " score : %f", can.label_match_score)
        '''

        # 5) for the remaing candidates get the abstract description(Entity type)
        candidate_list = []
        for can in remove_duplicates(same_type_candidates):
            get_abstract(can, spql_domain, False)

            if can.db_abstract != "":
                #Check for name similarity
                get_candidate_name_score(entity_name, can)
                if( can.name_match_score > 0.5):
                    candidate_list.append(can)
                #print("ID : " + can.fb_id + " name match score : %f", can.name_match_score)

       # 6) check text similarty
        #if empty return first form elastic search
        if not candidate_list:
            #print ("no candidates")
            candidate_entities[0].fb_id
            return
        #if only one return that one
        if( len(candidate_list) == 1 ):
            #print("The best match is "+ candidate_list[0].fb_id)
            return candidate_list[0].fb_id
        #calculate the best from abstract similarity
        else:
            best_score = 0.0
            best_fb_id = ""
            for can  in candidate_list:
                get_abstract_nouns(can, wordnet_lib, stopwords_lib)
                can.abstract_match_score = similar(can.abstract_nouns, entity_text)

                if(best_score < can.abstract_match_score):
                    best_score = can.abstract_match_score
                    best_fb_id = can.fb_id
                #print("ID : " + can.fb_id + " score : %f", can.abstract_match_score)

            #print("The best match is "+ best_fb_id)
            return best_fb_id
    else:
        #print("No candidates")
        return 0


if __name__ == '__main__':
    import sys
    try:
        _, ELS_DOMAIN, SPQL_DOMAIN, ENT_NAME, ENT_TYPE, ENT_TEXT, WORDNET_LIB = sys.argv
    except Exception as e:
        print('Error: '+ str(e))

        sys.exit(0)

    # execute linking
    link_entity(ELS_DOMAIN, SPQL_DOMAIN, ENT_NAME, ENT_TYPE, ENT_TEXT.split(" "), WORDNET_LIB)
