# Extracts a warc gz and retrieves each record
import warc, re
import sys
from bs4 import BeautifulSoup
import preprocessing
from entity_query import link_entity
from nltk.corpus import wordnet, stopwords
#import threading

def return_text(html_doc):
    soup = BeautifulSoup(html_doc, "html.parser")
    data = soup.findAll(text=True)

    def visible(element):
        if element.parent.name in ['style', 'script', '[document]', 'head', 'title', 'meta', 'link']:
            return False
        elif re.match('<!--.*-->', str(element.encode('utf-8'))):
            return False
        elif element == " ":
            return False
        return True

    result = filter(visible, data)
    result = (" ".join(result))
    return result

def remove_headers(doc_input):
    lines = doc_input.splitlines()
    start_reading = False
    html_text = ""
    for line in lines:
        if start_reading == False:
            if line == "":
                start_reading = True
        else:
            html_text += line
    html_text1 = return_text(html_text)
    #print(html_text1)
    #print
    #print("========================================")
    return html_text1

#Each thread is responsible for one document
def worker(text, lock, wordnet, stopwords, fb_id):

    doc = remove_headers(text)

    #preprocessing for non empty documents
    if doc:
        document_info = preprocessing.preprocess(doc, wordnet, stopwords)

        #get nouns form test
        entity_text = [noun.encode("utf-8") for noun in document_info.nouns];

        #get entities and their types and call link_entity
        for num in range(len(document_info.entities)):
            entity_name = document_info.entities[num][0].encode("utf-8")
            entity_type = document_info.entities[num][1].encode("utf-8")
            fb_id = link_entity(ELS_DOMAIN, SPQL_DOMAIN, entity_name, entity_type, entity_text, wordnet, stopwords)
            if fb_id is None:
                fb_id = "00000000"
            #CRITICAL SECTION.... lock to prevent concurrent printing from threads
            lock.acquire()
            print doc_id, "    ", entity_name, "    ", fb_id
            lock.release()


if __name__ == '__main__':

    try:
        _, ELS_DOMAIN, SPQL_DOMAIN, FILENAME = sys.argv
    except Exception as e:
        print('Error: '+ str(e))

    try:
        arc = warc.open(FILENAME)
        i = 0
        # we need to ensure that the LazyCorpusLoader attribute of the WordNetCorpusReader and WordListCorpusReader objects
        # of nltk.wordnet and nltk.stopwords will continue to exist even after the first thread
        wordnet.ensure_loaded()
        stopwords.ensure_loaded()
        locker = threading.Lock()      #initialize locker
        threads = []

        for record in arc:
            text = record.payload.read()
            #At some point we'll need the record id for the results
            wid = record["WARC-Record-ID"]
            doc_id = wid.split(":")
            doc_id = doc_id[2]
            doc_id = doc_id.split(">")
            doc_id = doc_id[0]

            #if text is empty go to the next document
            if not text:
                continue
                
            worker(text, locker, wordnet, stopwords, doc_id)

    except IOError:
        print("Error opening file")
