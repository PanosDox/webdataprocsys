#Vocabulary : https://www.w3.org/1999/02/22-rdf-syntax-ns#

echo "Vocabulary : https://www.w3.org/1999/02/22-rdf-syntax-ns#" 

python3 sparql.py $1:$2 "select * where {<http://rdf.freebase.com/ns/m.01cx6d_> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?o} limit 100"

echo "============="

#Vocabulary : https://www.w3.org/2000/01/rdf-schema#

echo "============="

echo "Vocabulary : https://www.w3.org/2000/01/rdf-schema#"

python3 sparql.py $1:$2 "select * where {<http://rdf.freebase.com/ns/m.01cx6d_> <http://www.w3.org/2000/01/rdf-schema#label> ?o} limit 100"

echo "============="

#Vocabulary : dublin core

echo "Vocabularies :  http://dbpedia.org/ontology/"

python3 sparql.py $1:$2 "select * where {<http://rdf.freebase.com/ns/m.01cx6d_> <http://dbpedia.org/ontology/abstract> ?o} limit 100"

echo "============="

query="select ?abstract where {  \
  ?s <http://www.w3.org/2002/07/owl#sameAs> <http://rdf.freebase.com/ns/m.01cx6d_> .  \
  ?s <http://www.w3.org/2002/07/owl#sameAs> ?o . \
  ?o <http://dbpedia.org/ontology/abstract> ?abstract . \
}"

python3 sparql.py $1:$2 "$query"




