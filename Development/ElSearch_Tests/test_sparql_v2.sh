python3 sparql.py $1:$2 "select * where {<http://rdf.freebase.com/ns/m.01cx6d_> ?p ?o} limit 100"


query="select * where {\
  ?s <http://www.w3.org/2002/07/owl#sameAs> <http://rdf.freebase.com/ns/m.0k3p> .\
  ?s <http://www.w3.org/2002/07/owl#sameAs> ?o .}"
python3 sparql.py $1:$2 "$query"

query="select ?abstract where {  \
  ?s <http://www.w3.org/2002/07/owl#sameAs> <http://rdf.freebase.com/ns/m.0k3p> .  \
  ?s <http://www.w3.org/2002/07/owl#sameAs> ?o . \
  ?o <http://dbpedia.org/ontology/abstract> ?abstract . \
  FILTER(langMatches(lang(?abstract), "en"))
}"
python3 sparql.py $1:$2 "$query"
