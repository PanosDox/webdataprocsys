import requests


def getClosestCandidates(domain, query):
    url = 'http://%s/freebase/label/_search' % domain
    response = requests.get(url, params={'q': query, 'size':1000})
    data_json = response.json()
    maximum_score = data_json["hits"]["max_score"]
    candidates = data_json["hits"]["hits"]
    closest_candidates = []
    for candidate in candidates:
        if candidate["_score"] == maximum_score:
            closest_candidates.append(candidate)
        else:
            break
    print(closest_candidates)


if __name__ == '__main__':
    import sys
    try:
        _, DOMAIN, QUERY = sys.argv
    except Exception as e:
        print('Usage: python kb.py DOMAIN QUERY')
        sys.exit(0)
    getClosestCandidates(DOMAIN, QUERY)