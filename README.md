WDPS assignment
Group 15

The directory "Development/src/" has all the important scripts for the assignment. 
You can find them in DAS (/home/wdps1815/scratch/linking_project/webdataprocsys/Development/Deliverables/Solution_NonScalable) 

"entity_linker.sh" : the shell script that run the elastic_search and trident in different DAS nodes, 
					 activates the virtual environment for python 2.7 (venv2.7) and run the all the scripts for the linking process. 
					 Needs a warc file as an input
					 
			
			how to execute (on DAS): bash entity_linker.sh ../sample.warc.gz > output.tsv
					 
		
		

Notes:  
1) The scipt can be executed on DAS (see the path above) by using the virtual environment in python 2.7 which is at the following path: /home/wdps1815/scratch/venv_2.7. 
2) At the virtual environment are installed all the libraries that we needed to test our solution.
3) On DAS there is also a subfolder (/Development/Deliverables/Spark_Solution) that contains the Spark solution code (unimplemented)

					 